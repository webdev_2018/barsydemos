<?php

include(__DIR__ . '\barsy_api_client.class.php');

/* Зарежда необходимите настройки - host, user, password*/
include(__DIR__ . '\examples\_example_settings.php');

$bapi = new Barsy_api_client($host, $user_name, $password, $conn_options);

$filters = new stdClass();  /*Налични филтри за списъка с артикули*/

$extra_properties = new stdClass();  /*Искане на допълнителни данни за всеки артикул. Може да се подадат няколко елемента*/

$offset = 0; /* Номер на ред от който да бъде започнат списъка. Използва се при странициране, когато трябва да се прескочат Х записа (по подразбиране: {празен стринг}) */
$length = 1000; /* Брой върнати редове в една заявка (по подразбиране: 1000) */

/* създаване на barsy action обект */
$bact = Barsy_api_action::create('Articles_GetList')
    ->setParam('extra_properties', Array('article_id', 'public', 'master_cat'))
    ->setParam('offset', $offset)
    ->setParam('length', $length);

/* Изпълняване на екшъна */
$depots = $bapi->run($bact);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="row">
    <div class="col-md-10 offset-md-1 py-4">
        <table class="table">
            <thead>
            <tr>
                <th scope="col"># Article ID</th>
                <th scope="col">Article Name</th>
                <th scope="col">Article Price</th>
                <th scope="col">Article Category</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($depots as $obj) {
                $row = "<tr><td>$obj->article_id</td><td>$obj->article_name</td><td>";
                $row .= number_format($obj->actual_price, 2);
                $row .= "</td><td>$obj->master_cat_name</td></tr>";
                echo $row;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>

