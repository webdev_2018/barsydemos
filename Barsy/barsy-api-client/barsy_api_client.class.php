<?php

/**
 * Class Barsy_api_client
 * Служи за връзка с Barsy API
 */
class Barsy_api_client{
  public $host;
  public $user;
  public $password;
  public $headers;
  public $proxy_host;
  public $proxy_port = 8888;
  public $request_type = 'json';
  public $response_type = 'json';
  public $socket_timeout = 20;
  public $exec_time = false;
  public $response_size = false;
  public $file_name = null;
  private $addition_params = null;
  public $debug = 0;
  public $result;
  public $max_redirects = 10;

  public function __construct($host=null, $user=null, $password=null, $options = array()){
    $this->set_options($host, $user, $password, $options);
    $this->headers['accept'] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
  }

  /**
   * @param null $host
   * @param null $user
   * @param null $password
   * @param array $options
   * @return Barsy_api_client
   */
  static function getCurrent($host=null, $user=null, $password=null, $options = array()){
    static $client;
    if(!$client){
      $client = new Barsy_api_client();
    }
    $client->set_options($host, $user, $password, $options);
    return $client;
  }

  public function set_options($host, $user, $password, $options = array()){
    if($host !== null){
      $this->host = rtrim($host,'/');
    }
    if($user !== null){
      $this->user = $user;
    }
    if($password !== null){
      $this->password = $password;
    }
    if(isset($options['proxy_host'])){
      $this->proxy_host = $options['proxy_host'];
    }
    if(isset($options['proxy_port'])) {
      $this->proxy_port = $options['proxy_port'];
    }
    if(isset($options['response_type'])){
      $this->response_type = $options['response_type'];
    }
    if(isset($options['request_type'])) {
      $this->request_type = $options['request_type'];
    }
    if(isset($options['socket_timeout'])) {
      $this->socket_timeout = $options['socket_timeout'];
    }
    if(isset($options['max_redirects'])) {
      $this->max_redirects = $options['max_redirects'];
    }
  }

  public function call($body,$response_type=false, $options = array()){

    if($this->request_type){
      $this->headers['content-type'] = 'Content-Type: application/'.$this->request_type.'; charset=UTF-8';
    }

    if($this->user){
      $this->headers['authorization'] = 'Authorization:Basic '.base64_encode("$this->user:$this->password");
    }else{
      unset($this->headers['authorization']);
    }
    $headers = $this->headers;
    if ($options && isset($options['headers']) && $options['headers']) {
      foreach($options['headers'] as $header => $hval) {
        $headers[$header] = $hval;
      }
    }
    if($this->debug){
      $headers['debug'] = 'LUKANET_DEBUG: 1';
    }

    $time = microtime(true);
    $response_type = $response_type?$response_type:$this->response_type;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $this->host."/endpoints/".$response_type.$this->getAdditionParams());
    if($body !== ''){
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
    }

    curl_setopt($ch, CURLOPT_TIMEOUT,$this->socket_timeout);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_PROTOCOLS,  CURLPROTO_HTTP|CURLPROTO_HTTPS);
    curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.pem');

    if($this->proxy_host != ''){
      curl_setopt($ch, CURLOPT_PROXY, $this->proxy_host.':'.$this->proxy_port);
    }

    foreach ($options as $okey => $oval) {
      if (strncmp($okey, 'CURLOPT_', 8) === 0) {
        curl_setopt($ch, constant($okey), $oval);
      }
    }

    $response = curl_exec($ch);

    if($response === false){
      throw new Barsy_api_client_message('Connection problem  with '.$this->host."/endpoints/".$response_type.' : '.curl_error($ch),'110'.sprintf("%02d", curl_errno($ch)));
    }

    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $try = 0;
    while (in_array($result['http_code'], array(301, 302, 303)) && $try < $this->max_redirects) { // Ако имаме редирект пробваме пак
      $try++;
      $new_location = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
      if(!$new_location && preg_match('/Location: (.*)/', $response, $new_location)){
        $new_location = $new_location[1];
      }
      if($new_location) {
        curl_setopt($ch, CURLOPT_URL, trim($new_location));
        $response = curl_exec($ch);
        if($response === false){
          throw new Barsy_api_client_message('Connection problem with ' . $new_location . ' : '.curl_error($ch), '110' . sprintf("%02d", curl_errno($ch)));
        }
        // Get the code again
        $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      }
    }
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr( $response, $header_size );
    curl_close($ch);
    $this->exec_time = round(microtime(true) - $time,3);
    $this->response_size = strlen($result['body']);

    if(preg_match('/Content-Disposition: inline; filename=(.*)/',$result['header'],$res)){
      $this->file_name = trim($res[1]);
    }

    $result['header'] = explode(PHP_EOL, $result['header']);
    foreach($result['header'] as $k=>$header){
      $t = explode(':',$header,2);
      if(sizeof($t)> 1){
        $headers[$t[0]] = trim($t[1]);
      }
    }
    $result['header'] = $headers;
    $this->result = $result;

    if($result['http_code'] != 200){
      $xerr = false;
      if(isset($result['header']['X-Error'])){
        $xerr = $result['header']['X-Error'];
      }
      $message_codes = array(501, 401);
      if(in_array($result['http_code'], $message_codes)){
        throw new Barsy_api_client_message($result['body'],$xerr);
      }else{
        throw new Barsy_api_client_fault($result['body'],$xerr);
      }
    }

    if($response_type == 'json'){
      $response = json_decode($result['body']);

      if($response === null){
        $error = 'Invalid JSON responce';
        if(function_exists('json_last_error')){
          switch (json_last_error()) {
            case JSON_ERROR_NONE:
              $error .= ' - No JSON errors';
              break;
            case JSON_ERROR_DEPTH:
              $error .= ' - JSON Maximum stack depth exceeded';
              break;
            case JSON_ERROR_STATE_MISMATCH:
              $error .= ' - JSON Underflow or the modes mismatch';
              break;
            case JSON_ERROR_CTRL_CHAR:
              $error .= ' - JSON Unexpected control character found';
              break;
            case JSON_ERROR_SYNTAX:
              $error .= ' - JSON Syntax error, malformed JSON';
              break;
            case JSON_ERROR_UTF8:
              $error .= ' - JSON Malformed UTF-8 characters, possibly incorrectly encoded';
              break;
            default:
              $error .= ' - JSON Unknown error';
              break;
          }
        }
        throw new Barsy_api_client_fault("Barsy API Error : ".$error);
      }

    }else{
      $response = $result['body'];
    }
    $this->result = $result;
    return $response;
  }

  function run($action,$response_type=false){
    return in_array($response_type,Array('res','pdf','html','xml','txt'))?$this->call($action,$response_type):current($this->call($action,$response_type));
  }

  function setBarsyId($barsy_id){
    $this->addition_params['bid'] = $barsy_id;
  }

  function setPosId($pos_id){
    $this->addition_params['pid'] = $pos_id;
  }

  private function getAdditionParams(){
    if($this->addition_params){
      return '?'.implode('&',$this->addition_params);
    }
  }
}

//------------------------------------------------------------------------------
class Barsy_api_action{

  function __construct($action_name){
    $this->{$action_name} = Array();
  }

  static function create($action_name){
    static $action;
    $action = new Barsy_api_action($action_name);
    return $action;
  }

  function setParam($name,$values){
    $this->{key(get_object_vars($this))} [$name] = $values;
    return $this;
  }
}

//------------------------------------------------------------------------------
class Barsy_api_client_error extends Exception{
  protected $clear_message;
  protected $error_prefix = '';

  public function getClearMessage() {
    return $this->clear_message;
  }
  public function __construct($message, $code = null) {
    $this->clear_message = $message;
    parent::__construct($this->error_prefix.$message, $code);
  }
}

//------------------------------------------------------------------------------
class Barsy_api_client_message extends Barsy_api_client_error{
  protected $error_prefix = "Barsy API Message: ";
  public function __construct($message, $code = null) {
    parent::__construct($message, $code);
  }
}

//------------------------------------------------------------------------------
class Barsy_api_client_fault extends Barsy_api_client_error{
  protected $error_prefix = "Barsy API Error: ";
  public function __construct($message, $code = null) {
    parent::__construct($message, $code);
  }
}
