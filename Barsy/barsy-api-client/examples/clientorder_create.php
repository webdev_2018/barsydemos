<?php
  /*    ~Примерно използване на barsy API ~
   * 
   *  Демонстрирана функция:
   *
   *     clientorders_create(object $order,array $rows);
   *
   *       Въздава нова КЛИЕНТСКА ПОРЪЧКА (различно от продажба) Връша номер на клиентската поръка
   *       
   *       $order - обект, съдържащ всички основни опции на клиентската поръчка
   *       $rows - масив, сърържащ редовете на поръчката
   *
    * Бележки:
   *    - примера може да се изпълни през конзола само с ">php clientorder_create.php" или през браузер
    *  - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
   *   
   * За допълнителни въпроси: helpme@lukanet.com
   */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);
  
  /* Описание на основните редове от поръчката */
  $order = new stdClass();
  //$order->order_num = 3547; /* Външен номер на поръчка - генериран от е-магазина */
  $order->client_id = 1;    /* ID на клиента по barsy - ако няма - записва се към Анонимен (ID:1) */
  //$order->client = {};    /* данни за нов клиент */
  $order->contact_name = "Васил Петров"; /* Лице за контакт/получаване - може да се различава от името на клиента */
  $order->client_email = "vasil@petrov.com";
  $order->client_tel  = "+359-88-7654321";
  $order->discount  = 0; /* Отстъпка върху цялата поръчка */
 
  $order->delivery_barsy_id = 1; /* ID на търговския обект, от който ще се вземе поръчката. Ако е пропуснато, се приема че ще се доставя до адрес, описан по-долу */
  $order->delivery_address = "гр.София ул. Крайбрежна N99, вход Д";
  $order->delivery_date = "2014-11-21 18:00:00"; /* Дата за доставка, може да съдържа и час */
  $order->delivery_date_until = "2014-11-21 19:00:00"; /* Крайна дата/час за доставка - оформя интервал за доставка - от 'delivery_date' до 'delivery_date_until' */

  $order->paymethod_id = 1; /* ID на начина на плащане по номенклатура на barsy */
  $order->description  = "Свободен текст"; /* Може да съдържа някакви допълнително описание към цялата поръчка */
  
  /* Редове от поръчката */
  $rows = Array();

  /* Описание на един ред от поръчката = артикул */
  $row = new stdClass();
  $row->article_id       = 100000; /* ID на артикул по номеклатура на barsy */
  $row->amount           = 2; /* Желано количество - може да бъде дробно число до 9-ти знак*/
  $row->current_price    = 2.30; /* Единична цена на поръчката - ако не е подадена, ще бъде ползвана текущата цена за този търговски обект/клиент/артикул */
  $row->detail_notes     = "Свободен текст"; /* Може да съдържа някакви допълнително описание към конкретния артикул */
      
  $rows[] = $row;  

  /* създаване на barsy action обект */
  $bact = Barsy_api_action::create('ClientOrders_Create')
             ->setParam('order',$order)
             ->setParam('rows',$rows);  
 
  /* Създаване на поръчката - връща ID на клиентската поръчка по номеклатурата на barsy */
  $client_order_id = $bapi->run($bact);

  echo "New client order : $client_order_id\n"; 
