<?php
  /**  ~ Примерно използване на barsy API ~
  * 
  *  Демонстрирана функция:
  *
  *    Files_getList(string $obj_type,int $obj_id,string $obj_property,string $file,[int $width])
  *
  *      Връша списък на всички файлове от дадена група (obj_property) за дадения артикул (obj_id)
  *  
  * Бележки:
  *  - примера може да се изпълни през конзола само с ">php sync_article_data.php" или през браузер
  *  - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
  *
  * За допълнителни въпроси: helpme@lukanet.com
  */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);

  /* създаване на barsy action обект */
  $bact = Barsy_api_action::create('Files_getList')
        ->setParam('obj_type','articles') 
        ->setParam('obj_id',100000)
        ->setParam('obj_property','PICTURE');
  
  $result = $bapi->run($bact);
  
  /* Визуализиране на резултата */
  print_r($result);
