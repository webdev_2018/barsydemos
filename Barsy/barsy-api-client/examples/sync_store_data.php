<?php
  /**  ~ Примерно използване на barsy API ~
  * 
  *  Демонстрирана функция:
  *
  *     sync_store_data($last_update=null,$depot_group_id=null,$оffset=0,$length=1000)
  *
  *       Връша всички артикули, чийто складови наличности са промени след
  *       подададената дата, филтрирани по дадена група складове. Всички параметри са опционални
  *       
  *       $last_update - дата на последна проверка (YYYY-MM-DD HH:MM:SS)
  *       $depot_group_id - ID на групата складове, по която да се филтрира. Служи за отделяне на продаваеми бройки от резервирани, бракувани и други наличности.
  *       $offset - брой записи, които да бъдат прескочени
  *       $length - брой записи в единичен рекуст. При нормална работа - тази цифра може да бъде голяма (например 10 000), но всичко зависи от общия брой артикули и скоростта на сървъра.
  *       
  *  Бележки:
  *   - примера може да се изпълни през конзола само с ">php test_client.php" или през браузер
  *   - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
  *   - има редове за записване на последното изпълнения във временен файл в tmp папка. Така може да бъде изпълняван автоматично от крон без допълнителна логика
  *   
  * За допълнителни въпроси: helpme@lukanet.com
  *
  */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  /* Примерен код за взимане на последното изпълнение */
  $last_run_file = sys_get_temp_dir().DIRECTORY_SEPARATOR.basename(__FILE__).".run";
  $last_run = file_exists($last_run_file)?date('Y-m-d H:i:s',fileatime($last_run_file)):date('Y-m-d H:i',strtotime('- 1000 minutes'));

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);

  /* създаване на barsy action обект */
  $bact = Barsy_api_action::create('Articles_GetList')
             ->setParam('filters',Array('amount_last_update'=> '>'.$last_run,'depot_group_id'=>1))
             ->setParam('extra_properties',Array('article_details','store_amount_all'))
             ->setParam('offset',0)
             ->setParam('length',1000);  
 
  $articles = $bapi->run($bact);

  /* Визуализиране на резултата */
  echo "Changed articles since $last_run are ".sizeof($articles)."\n"; 
  echo "List:\n";
  foreach($articles as $article){
    if(strtotime($last_run) < strtotime($article->create_date)){
      echo "New Article: $article->article_name\n";  
    }else{
      echo "Changed Article: $article->article_name\n";  
    }
  }
  
  /* Записване на последната проверка */
  touch($last_run_file);
