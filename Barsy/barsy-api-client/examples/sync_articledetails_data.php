<?php
  /**  ~ Примерно използване на barsy API ~
  * 
  *  Демонстрирана функция:
  *
  *   ArticleDetails_getTree()
  *     - Връша списък на всички детайли в системата, в "дървовиден" вид - група, детайл
  *
  *   ArticleDetails_getList()    
  *    - Връша списък на всички детайли в системата, в "списъчен" вид
  *
  *   ArticleDetails_getListValues(int $key_id)
  *    - Връша списък на всички стойности в системата за даден детайл

  *  
  * Бележки:
  *  - примера може да се изпълни през конзола само с ">php sync_article_data.php" или през браузер
  *  - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
  *
  * За допълнителни въпроси: helpme@lukanet.com
  */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);

  /* Изтегляне на наличните групи и детайли (дървориден вид) */
  $bact = Barsy_api_action::create('ArticleDetails_getTree');  

  $details = $bapi->run($bact);
  
  print_r($details);


  /* Изтегляне на наличните детайли - (списъчен вид) */
  $bact = Barsy_api_action::create('ArticleDetails_getList');  

  $details = $bapi->run($bact);
  
  print_r($details);

  
  /* Изтегляне на използваните стойности по заден ключ на детайл */
  $bact = Barsy_api_action::create('ArticleDetails_getListValues')
        ->setParam('key_id',1); 

  $details = $bapi->run($bact);
  
  print_r($details);