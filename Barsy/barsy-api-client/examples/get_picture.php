<?php
  /**  ~ Примерно използване на barsy API ~
  * 
  *  Демонстрирана функция:
  *
  *    Files_get(string $obj_type,int $obj_id,string $obj_property,string $file,[int $width])
  *
  *      Връша съдържанието на файл (снимка)
  *  
  * Бележки:
  *  - примера може да се изпълни през конзола само с ">php sync_article_data.php" или през браузер
  *  - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
  *
  * За допълнителни въпроси: helpme@lukanet.com
  */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);

  /* създаване на barsy action обект */
  $bact = Barsy_api_action::create('Files_get')
        ->setParam('obj_type','articles') 
        ->setParam('obj_id',100000)
        ->setParam('obj_property','PICTURE')
        ->setParam('file','1_bfb4d894c8163fda9b87677a4a7461c9.jpg')
        ->setParam('width',200);
  
  $result = $bapi->run($bact,'res');

  $tmp = sys_get_temp_dir().DIRECTORY_SEPARATOR.$bapi->file_name;
  /* Запис на резултата */
  file_put_contents($tmp,$result);

  echo "Saved to $tmp";