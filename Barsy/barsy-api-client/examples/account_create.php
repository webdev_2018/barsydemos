<?php
  /*    ~Примерно използване на barsy API ~
   * 
   *  Демонстрирана функция:
   *
   *     accounts_create(object $account,array $rows);
   *
   *       Въздава нова СМЕТКА/ПРОДАЖБА (различно от клиентска поръчка) Връша номер на сметка
   *       
   *       $account - обект, съдържащ всички основни опции на клиентската поръчка
   *       $rows - масив, сърържащ редовете на поръчката
   *
   * Бележки:
   *    - примера може да се изпълни през конзола само с ">php clientorder_create.php" или през браузер
   *    - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
   *   
   * За допълнителни въпроси: helpme@lukanet.com
   */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);
  
  /* Описание на основните редове от поръчката */
  $account = new stdClass();
  $account->client_id = 1;    /* ID на клиента по barsy - ако няма - записва се към Анонимен (ID:1) */
  $account->place_id = 1;     /* ID на място/маса/стая/...*/
  $account->person_id = null  /* ID на представител */;
  $account->discount = 0;     /* Отстъпка върху цялата поръчка */
  $account->paymethod_id = 1; /* ID на начина на плащане по номенклатура на barsy */

  $account->ref_date = "2014-11-21 18:00:00"; /* Дата на документално извършване на сделката */
  $account->persons = "2"; /* Крайна дата/час за доставка - оформя интервал за доставка - от 'delivery_date' до 'delivery_date_until' */
  $account->delivery_address = "гр.София ул. Крайбрежна N99, вход Д";
  $account->account_alias = "Заглавие на сметката"; /* ID на търговския обект, от който ще се вземе поръчката. Ако е пропуснато, се приема че ще се доставя до адрес, описан по-долу */
  $account->description  = "Свободен текст"; /* Може да съдържа някакви допълнително описание към цялата сметка - печати се на бележката */
  $account->notes = "Свободен текст"; /* Може да съдържа някакви допълнително описание към цялата сметка - НЕ се печати на бележката */
  
  /* Редове от поръчката */
  $rows = Array();

  /* Описание на един ред от поръчката = артикул */
  $row = new stdClass();
  $row->article_id       = 91638; /* ID на артикул по номеклатура на barsy */
  $row->amount           = 2; /* Желано количество - може да бъде дробно число до 9-ти знак*/
  $row->current_price    = 2.30; /* Единична цена на поръчката - ако не е подадена, ще бъде ползвана текущата цена за този търговски обект/клиент/артикул */
      
  $rows[] = $row;  

  /* създаване на barsy action обект */
  $bact = Barsy_api_action::create('Accounts_Create')
             ->setParam('account',$account)
             ->setParam('rows',$rows);  
 
  /* Създаване на сметката - връща ID на сметката по номеклатурата на barsy */
  $account_id = $bapi->run($bact);

  echo "New account: $account_id\n"; 
