<?php
  /**  ~ Примерно използване на barsy API ~
  * 
  *  Демонстрирана функция:
  *
  *    Articles_GetList(array $filters,array $extra_properties, int $offset=0, int $length=1000)
  *
  *      Връша всички артикули, чийто основни данни (име на артикул, цена, категория, описание, снимки и т.н.) ИЛИ складови данни (наличности по складове) са 
  *        промени след датата на последното синхронизиране.
  *  
  * Бележки:
  *  - примера може да се изпълни през конзола само с ">php sync_article_data.php" или през браузер
  *  - в случай на грешка, barsy API "хвърля" стандартен PHP Exception, който може да бъде хванат с try/catch блок
  *  - има редове за записване на последното изпълнения във временен файл в tmp папка. Така може да бъде изпълняван автоматично от крон без допълнителна логика
  *
  * За допълнителни въпроси: helpme@lukanet.com
  */
  
  include('../barsy_api_client.class.php'); 
  
  /* Зарежда необходимите настройки - host, user, password*/
  include('./_example_settings.php'); 

  $bapi = new Barsy_api_client($host,$user_name,$password,$conn_options);

  $bact = Barsy_api_action::create('Articles_GetList')
             ->setParam('filters',Array(
                                    'all_last_update'=> -1,
                                    'depot_group_id'=>1,
                                    'picture'=>true,
                                    'cat_id'=>true,
                                    )
                       )
             ->setParam('extra_properties',Array('base','article_details','store_amount_all'))
             ->setParam('offset',0)
             ->setParam('length',10);  
  
  $articles = $bapi->run($bact);

  $article_ids = Array();

  /* Визуализиране на резултата */
  echo "Changed articles are ".sizeof($articles)." time:".$bapi->exec_time."\n"; 
  if(sizeof($articles)){
    foreach($articles as $article){
      echo "[$article->article_id] $article->article_name\n";
      $article_ids[] = $article->article_id;
      /*
      * Актуализиране на артикула в локалната система
      * article_tra_la_la($article);
      */
    }
  }
  
  if(sizeof($article_ids)){
    /* Ако има обработени артикули - правим обратен рекуест, за да отбележим датата на последната им обработка */
    $bact = Barsy_api_action::create('ArticleDetails_setDetailValue')
               ->setParam('article_id',$article_ids)
               ->setParam('key_id',-1)
               ->setParam('detail_value',date('Y-m-d H:i:s')); 
    
    $articles = $bapi->run($bact);
  }
